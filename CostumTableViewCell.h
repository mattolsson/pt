//
//  CostumTableViewCell.h
//  PT-app
//
//  Created by MattiasO on 2015-04-07.
//  Copyright (c) 2015 Mattias Olsson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CostumTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *cellTitle;
@property (weak, nonatomic) IBOutlet UIImageView *cellImage;
@property (nonatomic) NSString *cellId;

@end
