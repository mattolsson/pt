//
//  LoginViewController.m
//  PT-app
//
//  Created by MattiasO on 2015-04-13.
//  Copyright (c) 2015 Mattias Olsson. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated {
    PFUser *user = [PFUser currentUser];
    if (user.username != nil) {
        [self performSegueWithIdentifier:@"loginSegue" sender:self];
    }
}

- (IBAction)loginButton:(id)sender {
    
    [PFUser logInWithUsernameInBackground:self.loginUsernameField.text password:self.loginPasswordField.text block:^(PFUser *user, NSError *error) {
        if (!error) {
            NSLog(@"Login user");
            self.loginUsernameField.text = @"";
            self.loginPasswordField.text = @"";
            [self dismissViewControllerAnimated:YES completion:nil];
            [self performSegueWithIdentifier:@"loginSegue" sender:self];
        }else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please try again" message:@"The username or password is incorrect" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }];
}
@end
