//
//  ProfileViewController.m
//  PT-app
//
//  Created by MattiasO on 2015-04-13.
//  Copyright (c) 2015 Mattias Olsson. All rights reserved.
//

#import "ProfileViewController.h"

@interface ProfileViewController ()

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)logoutButton:(id)sender {
    [PFUser logOut];
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController.navigationController popToRootViewControllerAnimated:YES];
}


@end
