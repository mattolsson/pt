//
//  LoginViewController.h
//  PT-app
//
//  Created by MattiasO on 2015-04-13.
//  Copyright (c) 2015 Mattias Olsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface LoginViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *loginUsernameField;
@property (weak, nonatomic) IBOutlet UITextField *loginPasswordField;

- (IBAction)loginButton:(id)sender;

@end
