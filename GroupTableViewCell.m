//
//  GroupTableViewCell.m
//  PT-app
//
//  Created by MattiasO on 2015-04-25.
//  Copyright (c) 2015 Mattias Olsson. All rights reserved.
//

#import "GroupTableViewCell.h"

@implementation GroupTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
