//
//  PickTableViewController.m
//  PT-app
//
//  Created by MattiasO on 2015-04-25.
//  Copyright (c) 2015 Mattias Olsson. All rights reserved.
//

#import "PickTableViewController.h"

@interface PickTableViewController ()

@end

@implementation PickTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self fetchFromParse];
}

-(void)fetchFromParse {
    PFQuery *query = [PFQuery queryWithClassName:@"Groups"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if(!error) {
            self.groupArray = [[NSArray alloc]initWithArray:objects];
        }
        [self.tableView reloadData];
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.groupArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    GroupTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PickCell" forIndexPath:indexPath];
    
    PFObject *obj = [self.groupArray objectAtIndex:indexPath.row];
    cell.groupId = obj.objectId;
    cell.groupLabel.text = [obj objectForKey:@"CellTitle"];
    
    
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"groupSegue"]) {
        TableViewController *table = [segue destinationViewController];
        GroupTableViewCell *cellNr = sender;
        table.groupString = cellNr.groupId;
        NSLog(@"Cell ID: %@", cellNr.groupId);
    }
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


@end
