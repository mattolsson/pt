//
//  PickTableViewController.h
//  PT-app
//
//  Created by MattiasO on 2015-04-25.
//  Copyright (c) 2015 Mattias Olsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "CostumTableViewCell.h"
#import "TableViewController.h"
#import "GroupTableViewCell.h"

@interface PickTableViewController : UITableViewController


@property (nonatomic) NSArray *groupArray;
@property (nonatomic) NSString *groupPick;

@end
