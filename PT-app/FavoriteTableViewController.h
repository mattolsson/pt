//
//  FavoriteTableViewController.h
//  PT-app
//
//  Created by MattiasO on 2015-04-24.
//  Copyright (c) 2015 Mattias Olsson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavoriteTableViewController : UITableViewController

@end
