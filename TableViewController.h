//
//  TableViewController.h
//  PT-app
//
//  Created by MattiasO on 2015-04-14.
//  Copyright (c) 2015 Mattias Olsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "CostumTableViewCell.h"
#import "DetailViewController.h"
#import "PickTableViewController.h"


@interface TableViewController : UITableViewController

@property (nonatomic) NSArray *workoutArray;
@property (nonatomic) NSString *groupString;
@property (nonatomic) NSString *queryPath;

@end
