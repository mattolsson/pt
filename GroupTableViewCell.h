//
//  GroupTableViewCell.h
//  PT-app
//
//  Created by MattiasO on 2015-04-25.
//  Copyright (c) 2015 Mattias Olsson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroupTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *groupLabel;

@property (nonatomic) NSString *groupId;

@end
