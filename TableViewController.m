//
//  TableViewController.m
//  PT-app
//
//  Created by MattiasO on 2015-04-14.
//  Copyright (c) 2015 Mattias Olsson. All rights reserved.
//

#import "TableViewController.h"

@interface TableViewController ()

@end

@implementation TableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getQueryPath];
    [self fetchFromParse];
}

-(void)getQueryPath {
    if ([self.groupString isEqualToString:@"PUpYCLeCqT"]) {
        self.queryPath = @"HoleBody";
    }else if ([self.groupString isEqualToString:@"6SeHdyyLob"]) {
        self.queryPath = @"LowerBody";
    } else {
        self.queryPath = @"WorkoutArray";
    }
}

-(void)fetchFromParse {
    
    PFQuery *query = [PFQuery queryWithClassName:self.queryPath];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if(!error) {
            self.workoutArray = [[NSArray alloc]initWithArray:objects];
        }
        [self.tableView reloadData];
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.workoutArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CostumTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"MyCell" forIndexPath:indexPath];
    
    PFObject *obj = [self.workoutArray objectAtIndex:indexPath.row];
    cell.cellTitle.text = [obj objectForKey:@"CellTitle"];
    cell.cellId = obj.objectId;
    PFFile *imageFile = [obj objectForKey:@"CellImage"];
    [imageFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
        if (!error) {
            cell.cellImage.image =[UIImage imageWithData:data];
        }
    }];
         
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"detailSegue"]) {
        CostumTableViewCell *cellNr = sender;
        DetailViewController *detail = [segue destinationViewController];
        detail.trainingId = cellNr.cellId;
        detail.queryPath = self.queryPath;
        NSLog(@"Cell ID: %@", cellNr.cellId);
    }
}


/*
 Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
     Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
 Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
         Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
         Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
 Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
 Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
     Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


@end
