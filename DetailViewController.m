//
//  DetailViewController.m
//  PT-app
//
//  Created by MattiasO on 2015-04-22.
//  Copyright (c) 2015 Mattias Olsson. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController () {
    MPMoviePlayerController *moviePlayer;
}

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadData];
}

-(void)viewDidAppear:(BOOL)animated {
    [moviePlayer play];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadData {
    PFQuery *query = [PFQuery queryWithClassName:self.queryPath];
    [query getObjectInBackgroundWithId:self.trainingId block:^(PFObject *object, NSError *error) {
        
        PFFile *movieFile = [object objectForKey:@"Videos"];
        NSURL *movieUrl = [NSURL URLWithString:movieFile.url];
        moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:movieUrl];
        [moviePlayer.view setFrame:self.videoView.bounds];
        [self.videoView addSubview:moviePlayer.view];
        
        moviePlayer.controlStyle = MPMovieControlStyleDefault;
        [moviePlayer prepareToPlay];
        
        if(!error) {
            self.testLabel.text = [object objectForKey:@"CellTitle"];
        }else {
            NSLog(@"error");
        }
    
    }];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if(moviePlayer.isFullscreen) {
        
    }else{
        [moviePlayer stop];
    }
}


@end
