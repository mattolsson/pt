//
//  SignUpViewController.m
//  PT-app
//
//  Created by MattiasO on 2015-04-10.
//  Copyright (c) 2015 Mattias Olsson. All rights reserved.
//

#import "SignUpViewController.h"

@interface SignUpViewController ()

@end

@implementation SignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)checkInput {
    if ([self.usernameField.text isEqualToString:@""] || [self.emailField.text isEqualToString:@""] || [self.password1Field.text isEqualToString:@""] || [self.password2Field.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please try again" message:@"Not all fields have been entered" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }else {
        [self checkPasswords];
    }
}

-(void)checkPasswords {
    if (![self.password1Field.text isEqualToString:self.password2Field.text]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please try again" message:@"Make sure your passwords match" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }else {
        [self regNewUser];
    }
}

-(void)regNewUser {
    NSLog(@"loading...");
    PFUser *newUser = [PFUser user];
    newUser.username = self.usernameField.text;
    newUser.email = self.emailField.text;
    newUser.password = self.password1Field.text;
    [newUser signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            NSLog(@"Registration successfull");
            self.usernameField.text = @"";
            self.emailField.text = @"";
            self.password1Field.text = @"";
            self.password2Field.text = @"";
            [self dismissViewControllerAnimated:YES completion:nil];
        }else {
            NSLog(@"Error in registration");
            if (error.code == 202) {
                self.errorMessage = @"Username already taken";
            }else if(error.code == 203) {
                self.errorMessage = @"Email already in use";
            }else if(error.code == 125) {
                self.errorMessage = @"Invalid email address";
            }
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please try again" message:self.errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];

        }
    }];
}

- (IBAction)signUpButton:(id)sender {
    [self.usernameField resignFirstResponder];
    [self.emailField resignFirstResponder];
    [self.password1Field resignFirstResponder];
    [self.password2Field resignFirstResponder];
    
    [self checkInput];

}

- (IBAction)closeButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
