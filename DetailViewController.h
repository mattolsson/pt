//
//  DetailViewController.h
//  PT-app
//
//  Created by MattiasO on 2015-04-22.
//  Copyright (c) 2015 Mattias Olsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "TableViewController.h"
#import <MediaPlayer/MediaPlayer.h>

@interface DetailViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *starButton;
@property (weak, nonatomic) IBOutlet UILabel *testLabel;
@property (weak, nonatomic) IBOutlet UIView *videoView;
@property (nonatomic) NSString *trainingId;
@property (nonatomic) NSString *queryPath;

@end
