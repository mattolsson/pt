//
//  SignUpViewController.h
//  PT-app
//
//  Created by MattiasO on 2015-04-10.
//  Copyright (c) 2015 Mattias Olsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface SignUpViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *usernameField;
@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property (weak, nonatomic) IBOutlet UITextField *password1Field;
@property (weak, nonatomic) IBOutlet UITextField *password2Field;

@property (nonatomic) NSString *errorMessage;

- (IBAction)signUpButton:(id)sender;
- (IBAction)closeButton:(id)sender;

@end
